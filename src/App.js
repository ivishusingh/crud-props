import React, { Component } from "react";

import "./App.css";

class App extends Component {
  state = {
    items: ["hii", "hii1122"],
    term: ""
  };
  InputHandler = e => {
    let res = e.target.value;
    this.setState({ term: res });
  };
  AddItems = () => {
    let z = [...this.state.items];
    z.push(this.state.term);
    this.setState({ items: z });
  };

  DeleteItem = () => {
    let del = [...this.state.items];
    del.pop();
    this.setState({ items: del });
  };
  DeleteCurrent = index => {
    let del = [...this.state.items];
    del.splice(index, 1);
    this.setState({ items: del });
  };

  EditItem = index => {
    
  };

  render() {
    return (
      <div className="App">
        <input onChange={this.InputHandler} type="text" />
        <button onClick={this.AddItems}>Submit</button>
        <button onClick={this.DeleteItem}>Delete Last</button>

        <h2>
          {this.state.items.map((k,index) => (
            <li>
              {k}
              <button onClick={() => this.DeleteCurrent(index)}>Delete</button>
              <button onClick={() => this.EditItem(index)}>Edit</button>
            </li>
          ))}
        </h2>
      </div>
    );
  }
}

export default App;
